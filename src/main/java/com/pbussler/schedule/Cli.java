package com.pbussler.schedule;

import com.pbussler.schedule.entity.Employee;
import com.pbussler.schedule.entity.Shift;
import org.hibernate.Session;

import javax.persistence.NoResultException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Cli {

	private static SimpleDateFormat DEFAULT_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.US);

	private static Session session = SessionManager.getSessionFactory().openSession();
	private static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

	private static void seedDB() {
		Date date1;
		Date date2;
		Date date3;
		Date date4;
		Date date5;
		Date date6;
		Date date7;

		try {
			date1 = DEFAULT_DATE_FORMAT.parse("2018-11-07 08:30");
			date2 = DEFAULT_DATE_FORMAT.parse("2018-11-07 10:30");
			date3 = DEFAULT_DATE_FORMAT.parse("2018-11-07 12:30");
			date4 = DEFAULT_DATE_FORMAT.parse("2018-11-07 14:30");
			date5 = DEFAULT_DATE_FORMAT.parse("2018-11-07 16:30");
			date6 = DEFAULT_DATE_FORMAT.parse("2018-11-07 18:30");
			date7 = DEFAULT_DATE_FORMAT.parse("2018-11-07 20:30");
		} catch (ParseException e) {
			// Should never get here, dates are static strings in the correct format
			throw new RuntimeException("Failed to parse date from string." + e);
		}

		Employee emp1 = Employee.create(session, "Jill", "Anderson", Role.manager);
		Employee emp2 = Employee.create(session, "Jack", "Johnson", Role.individual);
		Employee emp3 = Employee.create(session, "John", "Smith", Role.individual);

		Shift.create(session, emp1, date1, date4);
		Shift.create(session, emp1, date6, date7);

		Shift.create(session, emp2, date2, date3);
		Shift.create(session, emp2, date6, date7);

		Shift.create(session, emp3, date1, date3);
		Shift.create(session, emp3, date4, date5);
	}

	public static void main(String[] args) throws IOException {

		seedDB();

		String input;
		do {
			System.out.println("Commands: \n" +
					"  list shifts, view shift, create shift, edit shift, delete shift\n" +
					"\n" +
					"  list employees, view employee, create employee, edit employee, delete employee\n" +
					"\n" +
					"  exit\n");
			System.out.print("Enter Command:");
			input = br.readLine();

			switch (input) {
				case "list shifts":
					listShifts();
					break;
				case "view shift":
					viewShift();
					break;
				case "create shift":
					createShift();
					break;
				case "edit shift":
					editShift();
					break;
				case "delete shift":
					deleteShift();
					break;
				case "list employees":
					listEmployees();
					break;
				case "view employee":
					viewEmployee();
					break;
				case "create employee":
					createEmployee();
					break;
				case "edit employee":
					editEmployee();
					break;
				case "delete employee":
					deleteEmployee();
					break;
				case "exit":
					// Don't print invalid command...
					break;
				default:
					System.out.println("Invalid Command!");
			}

		} while (!input.contains("exit"));

		br.close();
		SessionManager.shutdown();
	}

	private static void listShifts() {
		System.out.println();
		Shift.getAll(session).forEach(System.out::println);
		System.out.println();
	}

	private static void viewShift() throws IOException {
		Shift shift = getShiftFromUser();
		if (shift != null) {
			System.out.println();
			System.out.println(shift);
			System.out.println();
		}
	}

	private static void createShift() throws IOException {
		Employee employee = getEmployeeFromUser();

		boolean inputAccepted = false;
		Date startTime = null;
		do {
			try {
				System.out.print("Enter start time (Format: yyyy-MM-DD HH:mm):");
				String input = br.readLine();
				startTime = DEFAULT_DATE_FORMAT.parse(input);
				inputAccepted = true;
			} catch (ParseException e) {
				System.out.println("Invalid date please use format yyyy-MM-DD HH:mm.");
			}
		} while (!inputAccepted);

		inputAccepted = false;
		Date endTime = null;
		do {
			try {
				System.out.print("Enter end time (Format: yyyy-MM-DD HH:mm):");
				String input = br.readLine();
				endTime = DEFAULT_DATE_FORMAT.parse(input);
				inputAccepted = true;
			} catch (ParseException e) {
				System.out.println("Invalid date please use format yyyy-MM-DD HH:mm.");
			}
		} while (!inputAccepted);

		try {
			System.out.println();
			System.out.println(Shift.create(session, employee, startTime, endTime));
			System.out.println();
		} catch (IllegalArgumentException e) {
			System.out.println("Failed to create shift: " + e.getMessage());
		}
	}

	private static void editShift() throws IOException {
		Shift shift = getShiftFromUser();

		String input;
		System.out.println("What do you want to edit: \n" +
				"  employee\n" +
				"  start time\n" +
				"  end time\n");
		System.out.print("Enter Command:");
		input = br.readLine();

		switch (input) {
			case "employee":
				Employee employee = getEmployeeFromUser();
				if (employee != null) {
					try {
						shift.setEmployee(session, employee);
					} catch (IllegalArgumentException e) {
						System.out.println(e.getMessage() + " Shift unchanged.");
						return;
					}
				}
				break;
			case "start time":
				Date startTime = getDateFromUser();

				if (startTime != null) {
					try {
						shift.setStart(session, startTime);
					} catch (IllegalArgumentException e) {
						System.out.println(e.getMessage() + " Shift unchanged.");
						return;
					}
				}
				break;
			case "end time":
				Date endTime = getDateFromUser();

				if (endTime != null) {
					try {
						shift.setStart(session, endTime);
					} catch (IllegalArgumentException e) {
						System.out.println(e.getMessage() + " Shift unchanged.");
						return;
					}
				}
				break;
			default:
				System.out.println("Invalid Command!");
		}
	}

	private static void deleteShift() throws IOException {
		Shift shift = getShiftFromUser();

		if (shift != null) {
			shift.remove(session);
		}
	}

	private static void listEmployees() {
		System.out.println();
		Employee.getAll(session).forEach(System.out::println);
		System.out.println();
	}

	private static void viewEmployee() throws IOException {
		Employee employee = getEmployeeFromUser();

		if (employee != null) {
			System.out.println();
			System.out.println(employee);
			System.out.println();
		}
	}

	private static void createEmployee() throws IOException {

		System.out.print("Enter first name:");
		String firstName = br.readLine();

		System.out.print("Enter last name:");
		String lastName = br.readLine();

		boolean inputAccepted = false;
		Role role = null;
		do {
			try {
				System.out.print("Enter role (manager | individual):");
				String roleInput = br.readLine();
				role = Role.valueOf(roleInput);
				inputAccepted = true;
			} catch (IllegalArgumentException e) {
				System.out.println("Invalid role.");
			}
		} while (! inputAccepted);

		System.out.println();
		System.out.println(Employee.create(session, firstName, lastName, role));
		System.out.println();
	}

	private static void editEmployee() throws IOException {
		Employee employee = getEmployeeFromUser();

		if (employee != null) {
			String input;
			System.out.println("What do you want to edit: \n" +
					"  first name\n" +
					"  last name\n" +
					"  role\n");
			System.out.print("Enter Command:");
			input = br.readLine();

			switch (input) {
				case "first name":
					System.out.print("Enter first name:");
					String firstName = br.readLine();
					employee.setFirstName(firstName);
					break;
				case "last name":
					System.out.print("Enter last name:");
					String lastName = br.readLine();
					employee.setLastName(lastName);
					break;
				case "role":
					boolean inputAccepted = false;
					Role role = null;
					do {
						try {
							System.out.print("Enter role (manager | individual):");
							String roleInput = br.readLine();
							role = Role.valueOf(roleInput);
							inputAccepted = true;
						} catch (IllegalArgumentException e) {
							System.out.println("Invalid role.");
						}
					} while (! inputAccepted);
					employee.setRole(role);
					break;
				default:
					System.out.println("Invalid Command!");
			}
		}
	}

	private static void deleteEmployee() throws IOException {
		Employee employee = getEmployeeFromUser();

		if (employee != null) {
			employee.remove(session);
		}
	}

	private static Shift getShiftFromUser() throws IOException {
		boolean inputAccepted = false;
		int shiftID = -1;
		do {
			try {
				System.out.print("Enter shift id:");
				String input = br.readLine();
				if (! input.isEmpty()) {
					shiftID = Integer.parseInt(input);
					inputAccepted = true;
				}
			} catch (NumberFormatException e) {
				System.out.println("Invalid id, please enter an integer.");
			}
		} while (!inputAccepted);

		Shift shift = null;
		try {
			shift = Shift.getById(session, shiftID);
		} catch (NoResultException e) {
			System.out.println("Shift not found.");
		}

		return shift;
	}

	private static Employee getEmployeeFromUser() throws IOException {
		boolean inputAccepted = false;
		int employeeID = -1;
		do {
			try {
				System.out.print("Enter employee ID:");
				String employeeInput = br.readLine();
				if (!employeeInput.isEmpty()) {
					employeeID = Integer.parseInt(employeeInput);
					inputAccepted = true;
				}
			} catch (NumberFormatException e) {
				System.out.println("Invalid id, please enter an integer.");
			}
		} while (!inputAccepted);

		Employee employee = null;
		try {
			employee = Employee.getById(session, employeeID);
		} catch (NoResultException e) {
			System.out.println("Employee not found.");
		}
		return employee;
	}

	private static Date getDateFromUser() throws IOException {
		boolean inputAccepted = false;
		Date date = null;
		do {
			try {
				System.out.print("Enter date/time (Format: yyyy-MM-DD HH:mm):");
				String endTimeInput = br.readLine();
				if (! endTimeInput.isEmpty()) {
					date = DEFAULT_DATE_FORMAT.parse(endTimeInput);
					inputAccepted = true;
				}
			} catch (ParseException e) {
				System.out.println("Invalid date please use format yyyy-MM-DD HH:mm.");
			}
		} while (!inputAccepted);
		return date;
	}

}
