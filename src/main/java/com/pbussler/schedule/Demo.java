package com.pbussler.schedule;

import com.pbussler.schedule.entity.Employee;
import com.pbussler.schedule.entity.Shift;
import org.hibernate.Session;

import javax.sound.midi.SysexMessage;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Hello world!
 */
public class Demo {

	private static SimpleDateFormat DEFAULT_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.US);

	public static void main(String[] args) {

		Session session = SessionManager.getSessionFactory().openSession();

		Date date1;
		Date date2;
		Date date3;
		Date date4;
		Date date5;
		Date date6;
		Date date7;

		try {
			date1 = DEFAULT_DATE_FORMAT.parse("2018-11-07 08:30");
			date2 = DEFAULT_DATE_FORMAT.parse("2018-11-07 10:30");
			date3 = DEFAULT_DATE_FORMAT.parse("2018-11-07 12:30");
			date4 = DEFAULT_DATE_FORMAT.parse("2018-11-07 14:30");
			date5 = DEFAULT_DATE_FORMAT.parse("2018-11-07 16:30");
			date6 = DEFAULT_DATE_FORMAT.parse("2018-11-07 18:30");
			date7 = DEFAULT_DATE_FORMAT.parse("2018-11-07 20:30");
		} catch (ParseException e) {
			// TODO: Handle this exception so that parsing dates fails gracefully
			throw new RuntimeException("Failed to parse date from string." + e);
		}

		Employee emp1 = Employee.create(session, "Jill", "Anderson", Role.manager);
		Employee emp2 = Employee.create(session, "Jack", "Johnson", Role.individual);
		Employee emp3 = Employee.create(session, "John", "Smith", Role.individual);

		Shift sft1 = Shift.create(session, emp1, date1, date4);
		Shift sft2 = Shift.create(session, emp1, date6, date7);

		Shift sft3 = Shift.create(session, emp2, date2, date3);
		Shift sht4 = Shift.create(session, emp2, date6, date7);

		Shift sft5 = Shift.create(session, emp3, date1, date3);
		Shift sft6 = Shift.create(session, emp3, date4, date5);

		System.out.println("All Employees:");
		for(Employee employee : Employee.getAll(session)) {
			System.out.println(employee);
		}

		System.out.println("All Shifts:");
		for(Shift shift : Shift.getAll(session)) {
			System.out.println(shift);
		}

		System.out.println("Shifts by Employee:");
		for(Shift shift : Shift.getByEmployee(session, emp1)) {
			System.out.println(shift);
		}

		System.out.println("Shifts by Start:");
		for(Shift shift : Shift.getByStartTime(session, date2)) {
			System.out.println(shift);
		}

		System.out.println("Shifts by End:");
		for(Shift shift : Shift.getByEndTime(session, date7)) {
			System.out.println(shift);
		}

		System.out.println("Assigning shift, " + sft6.getId() + " to employee, " + emp2.getId() + ". This should succeed.");
		try {
			sft6.setEmployee(session, emp2);
		} catch (IllegalArgumentException e) {
			System.out.println("FAILED: " + e.getMessage());
		}

		System.out.println("Assigning shift, " + sft5.getId() + " to employee, " + emp1.getId() + ". This should fail.");
		try {
			sft5.setEmployee(session, emp1);
		} catch (IllegalArgumentException e) {
			System.out.println("FAILED: " + e.getMessage());
		}

		System.out.println("Edit shift, " + sft1.getId() + " with new start time, " + date6 + ". This should fail.");
		try {
			sft1.setStart(session, date6);
		} catch (IllegalArgumentException e) {
			System.out.println("FAILED: " + e.getMessage());
		}

		System.out.println("Edit shift, " + sft1.getId() + " with new end time, " + date7 + ". This should fail.");
		try {
			sft1.setEnd(session, date7);
		} catch (IllegalArgumentException e) {
			System.out.println("FAILED: " + e.getMessage());
		}

		System.out.println("Delete shift," + sft3.getId());
		sft3.remove(session);

		System.out.println("All Shifts:");
		for(Shift shift : Shift.getAll(session)) {
			System.out.println(shift);
		}

		System.out.println("Delete employee, " + emp3);
		emp3.remove(session);

		System.out.println("All Employees:");
		for(Employee employee : Employee.getAll(session)) {
			System.out.println(employee);
		}

		System.out.println("All Shifts:");
		for(Shift shift : Shift.getAll(session)) {
			System.out.println(shift);
		}

		SessionManager.shutdown();
	}
}
