package com.pbussler.schedule.entity;

import com.pbussler.schedule.Role;
import org.hibernate.Session;
import org.hibernate.query.Query;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "Employee", uniqueConstraints = {@UniqueConstraint(columnNames = "ID")})
public class Employee implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	private Integer id;

	@Column(name = "FIRST_NAME", nullable = false, length = 100)
	private String firstName;

	@Column(name = "LAST_NAME", nullable = false, length = 100)
	private String lastName;

	@Column(name = "ROLE", nullable = false)
	private Role role;

	public Employee() {

	}

	public Employee(String firstName, String lastName, Role role) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.role = role;
	}

	public static Employee create(Session session, String  firstName, String lastName, Role role) {
		Employee createdEmployee = new Employee(firstName, lastName, role);
		session.beginTransaction();
		session.persist(createdEmployee);
		session.getTransaction().commit();
		return createdEmployee;
	}

	public Integer getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public void remove(Session session) {
		Shift.getByEmployee(session, this).forEach(shift -> shift.remove(session));
		session.beginTransaction();
		session.remove(this);
		session.getTransaction().commit();
	}

	public static Employee getById(Session session, int id) {
		Query<Employee> query = session.createQuery("select e from Employee e where e.id=:id", Employee.class);
		query.setParameter("id", id);
		return query.getSingleResult();
	}

	public static List<Employee> getAll(Session session) {
		return session.createQuery("select e from Employee e", Employee.class).list();
	}

	@Override
	public String toString() {
		return "Employee{" +
				"id=" + id +
				", firstName='" + firstName + '\'' +
				", lastName='" + lastName + '\'' +
				", role=" + role +
				'}';
	}
}
