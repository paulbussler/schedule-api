package com.pbussler.schedule.entity;

import org.hibernate.Session;
import org.hibernate.query.Query;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "Shift", uniqueConstraints = {@UniqueConstraint(columnNames = "ID")})
public class Shift implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	private Integer id;

	@OneToOne
	@JoinColumn(name = "EMPLOYEE_ID", nullable = false)
	private Employee employee;

	@Column(name = "START_TIME", nullable = false)
	private Date start;

	@Column(name = "END_TIME", nullable = false)
	private Date end;

	public Shift() {

	}

	public Shift(Employee employee, Date start, Date end) {
		this.employee = employee;
		this.start = start;
		this.end = end;
	}

	public static Shift create(Session session, Employee employee, Date start, Date end) throws IllegalArgumentException {
		if (end.before(start)) {
			throw new IllegalArgumentException("The start of a shift must be before the end.");
		}

		if (isOverlappingShift(getByEmployee(session, employee), start, end)) {
			throw new IllegalArgumentException("Specified start and end overlaps existing shift for employee, " + employee.getId() + ".");
		}

		Shift createdShift = new Shift(employee, start, end);
		session.beginTransaction();
		session.persist(createdShift);
		session.getTransaction().commit();

		return createdShift;
	}

	public Integer getId() {
		return id;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Session session, Employee employee) {
		if (isOverlappingShift(getByEmployee(session, employee), this.start, this.end)) {
			throw new IllegalArgumentException("Employee, " + employee.getId() + ", has an overlapping shift.");
		}
		this.employee = employee;
		persistSelf(session);
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Session session, Date start) {
		if (start.after(this.getEnd())) {
			throw new IllegalArgumentException("The start of a shift must be before the end.");
		}
		List<Shift> otherShifts = getByEmployee(session, this.employee);
		otherShifts.removeIf(s -> s.getId().equals(this.getId()));
		if (isOverlappingShift(otherShifts, start, this.end)) {
			throw new IllegalArgumentException("Employee, " + employee.getId() + ", has an overlapping shift for start time, " + start + ".");
		}
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void remove(Session session) {
		session.beginTransaction();
		session.remove(this);
		session.getTransaction().commit();
	}

	public void setEnd(Session session, Date end) {
		if (end.before(this.getStart())) {
			throw new IllegalArgumentException("The start of a shift must be before the end.");
		}
		List<Shift> otherShifts = getByEmployee(session, this.employee);
		otherShifts.removeIf(s -> s.getId().equals(this.getId()));
		if (isOverlappingShift(otherShifts, this.start, end)) {
			throw new IllegalArgumentException("Employee, " + employee.getId() + ", has an overlapping shift for end time, " + end + ".");
		}
		this.end = end;
	}

	private void persistSelf(Session session) {
		session.beginTransaction();
		session.persist(this);
		session.getTransaction().commit();
	}

	private static boolean isOverlappingShift(List<Shift> shifts, Date start, Date end) {
		for (Shift shift : shifts) {
			if (shift.getStart().before(end) && start.before(shift.getEnd())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Get all shifts from persistence
	 * @param session A session to retrieve from
	 * @return A list of Shifts
	 */
	public static List<Shift> getAll(Session session) {
		return session.createQuery("select s from Shift s order by s.start", Shift.class).list();
	}

	/**
	 * Get a shift from persistence by Id
	 * @param session A session to retrieve from
	 * @param id The id
	 * @return A Shift
	 */
	public static Shift getById(Session session, int id) {
		Query<Shift> query = session.createQuery("select s from Shift s where s.id=:id", Shift.class);
		query.setParameter("id", id);
		return query.getSingleResult();
	}

	/**
	 * Get all shifts of an employee from persistence
	 * @param session A session to retrieve from
	 * @param employee The employee
	 * @return A list of Shifts
	 */
	public static List<Shift> getByEmployee(Session session, Employee employee) {
		Query<Shift> query = session.createQuery("select s from Shift s where s.employee=:employee", Shift.class);
		query.setParameter("employee", employee);
		return query.list();
	}

	/**
	 * Get all shifts that start at a specified time from persistence
	 * @param session A session to retrieve from
	 * @param start The start time
	 * @return A list of Shifts
	 */
	public static List<Shift> getByStartTime(Session session, Date start) {
		Query<Shift> query = session.createQuery("select s from Shift s where s.start=:start", Shift.class);
		query.setParameter("start", start);
		return query.list();
	}

	/**
	 * Get all shifts that end at a specified time from persistence
	 * @param session A session to retrieve from
	 * @param end The end time
	 * @return A list of Shifts
	 */
	public static List<Shift> getByEndTime(Session session, Date end) {
		Query<Shift> query = session.createQuery("select s from Shift s where s.end=:end", Shift.class);
		query.setParameter("end", end);
		return query.list();
	}

	@Override
	public String toString() {
		return "Shift{" +
				"id=" + id +
				", employee=" + employee +
				", start=" + start +
				", end=" + end +
				'}';
	}
}
