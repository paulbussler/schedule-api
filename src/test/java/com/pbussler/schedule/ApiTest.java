package com.pbussler.schedule;

import com.pbussler.schedule.entity.Employee;
import com.pbussler.schedule.entity.Shift;
import org.hibernate.Session;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


/**
 * Unit test for Schedule API.
 */
public class ApiTest {

	private static SimpleDateFormat DEFAULT_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.US);

	private static Date date1;
	private static Date date2;
	private static Date date3;
	private static Date date4;
	private static Date date5;
	private static Date date6;
	private static Date date7;

	private static int emp1Id;
	private static int emp2Id;
	private static int emp3Id;
	private static int emp4Id;
	private static int emp5Id;

	private static int sft1Id;
	private static int sft2Id;
	private static int sft3Id;
	private static int sft4Id;
	private static int sft5Id;
	private static int sft6Id;
	private static int sft7Id;
	private static int sft8Id;
	private static int sft9Id;

	public ApiTest() {
	}

	@BeforeClass
	public static void initialSetup(){
		Session session = SessionManager.getSessionFactory().openSession();

		try {
			date1 = DEFAULT_DATE_FORMAT.parse("2018-11-12 08:30");
			date2 = DEFAULT_DATE_FORMAT.parse("2018-11-12 10:30");
			date3 = DEFAULT_DATE_FORMAT.parse("2018-11-12 12:30");
			date4 = DEFAULT_DATE_FORMAT.parse("2018-11-12 14:30");
			date5 = DEFAULT_DATE_FORMAT.parse("2018-11-12 16:30");
			date6 = DEFAULT_DATE_FORMAT.parse("2018-11-12 18:30");
			date7 = DEFAULT_DATE_FORMAT.parse("2018-11-12 20:30");
		} catch (ParseException e) {
			throw new RuntimeException("Failed to parse date from string." + e);
		}

		Employee emp1 = Employee.create(session, "Jill", "Anderson", Role.manager);
		Employee emp2 = Employee.create(session, "Jack", "Johnson", Role.individual);
		Employee emp3 = Employee.create(session, "John", "Smith", Role.individual);
		Employee emp4 = Employee.create(session, "Sara", "Jones", Role.individual);
		Employee emp5 = Employee.create(session, "Dan", "Wilson", Role.individual);

		emp1Id = emp1.getId();
		emp2Id = emp2.getId();
		emp3Id = emp3.getId();
		emp4Id = emp4.getId();
		emp5Id = emp5.getId();

		sft1Id = Shift.create(session, emp1, date1, date4).getId();
		sft2Id = Shift.create(session, emp1, date6, date7).getId();

		sft3Id = Shift.create(session, emp2, date2, date3).getId();
		sft4Id = Shift.create(session, emp2, date6, date7).getId();

		sft5Id = Shift.create(session, emp3, date1, date3).getId();
		sft6Id = Shift.create(session, emp3, date4, date5).getId();

		sft7Id = Shift.create(session, emp4, date1, date2).getId();
		sft8Id = Shift.create(session, emp4, date3, date4).getId();

		sft9Id = Shift.create(session, emp5, date5, date6).getId();
	}

	@AfterClass
	public static void finalTeardown(){
		SessionManager.shutdown();
	}

	@Test
	public void testGetAllEmployees() {
		Session session = SessionManager.getSessionFactory().openSession();

		assertThat(Employee.getAll(session).size(), is(greaterThan(0)));

		session.close();
	}

	@Test
	public void testGetAllShifts() {
		Session session = SessionManager.getSessionFactory().openSession();

		assertThat(Shift.getAll(session).size(),is(greaterThan(0)));

		session.close();
	}

	@Test
	public void testGetShiftByEmployee() {
		Session session = SessionManager.getSessionFactory().openSession();

		Employee emp = Employee.getAll(session).get(0);
		Shift.getByEmployee(session, emp).forEach(s -> assertThat(emp.getId(), is(equalTo(s.getEmployee().getId()))));

		session.close();
	}

	@Test
	public void testGetShiftByStartTime() {
		Session session = SessionManager.getSessionFactory().openSession();

		Shift.getByStartTime(session, date2).forEach(s -> assertThat(date2, is(equalTo(s.getStart()))));

		session.close();
	}

	@Test
	public void testGetShiftByEndTime() {
		Session session = SessionManager.getSessionFactory().openSession();

		Shift.getByEndTime(session, date7).forEach(s -> assertThat(date7, is(equalTo(s.getEnd()))));

		session.close();
	}

	@Test
	public void testAssignShiftToEmployee() {
		Session session = SessionManager.getSessionFactory().openSession();

		Employee emp1 = Employee.getById(session, emp1Id);
		Employee emp2 = Employee.getById(session, emp2Id);

		Shift sft5 = Shift.getById(session, sft5Id);
		Shift sft6 = Shift.getById(session, sft6Id);

		try {
			sft6.setEmployee(session, emp2);
		} catch (IllegalArgumentException e) {
			assertThat(e.getMessage(), false);
		}

		try {
			sft5.setEmployee(session, emp1);
			assertThat("Assigning overlapping shift should have failed.", false);
		} catch (IllegalArgumentException ignored) {}

		session.close();
	}

	@Test
	public void testEditShift() {
		Session session = SessionManager.getSessionFactory().openSession();

		Shift sft1 = Shift.getById(session, sft1Id);

		try {
			sft1.setStart(session, date6);
			assertThat("Assigning overlapping start time should have failed.", false);
		} catch (IllegalArgumentException ignored) {}

		try {
			sft1.setEnd(session, date7);
			assertThat("Assigning overlapping end time should have failed.", false);
		} catch (IllegalArgumentException ignored) {}

		session.close();
	}

	@Test
	public void testDeleteShift() {
		Session session = SessionManager.getSessionFactory().openSession();

		Shift sft9 = Shift.getById(session, sft9Id);

		int deletedShift = sft9.getId();

		sft9.remove(session);

		Shift.getAll(session).forEach(s -> assertThat(deletedShift, is(not(equalTo(s.getId())))));

		session.close();
	}

	@Test
	public void testDeleteEmployee() {
		Session session = SessionManager.getSessionFactory().openSession();

		Employee emp4 = Employee.getById(session, emp4Id);

		int deletedEmployee = emp4.getId();

		emp4.remove(session);

		Employee.getAll(session).forEach(e -> assertThat(deletedEmployee, is(not(equalTo(e.getId())))));

		Shift.getAll(session).forEach(s -> assertThat(deletedEmployee, is(not(equalTo(s.getEmployee().getId())))));

		session.close();
	}
}
