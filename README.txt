Schedule API

This programming example lays out a basic employee shift management API that allows you to create and manage employees and shifts.

It uses Hibernate, an ORM library, to manage backend persistence.  The database, HSQLDB, is in memory,
but could be swapped out for anything by changing the hibernate configuration and adding a different JDBC driver.

Normally I would have separated business logic from the data model, but I was experimenting with the concept that this design
breaks a lot of OO principles.  So, data model classes contain the logic needed to bound shift times and employees.
I found this to be easy to understand helped keep the logic from being scattered across multiple classes.  It reduced file complexity
but I can see merits in the separation model as well, in very large projects with lots of complex business logic data classes
could get very muddled.

To compile you need a JDK installed, download from https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html.
Then create a new windows environment variable JAVA_HOME pointing to the install directory.

The build tool is Maven.

Simply download Maven from their website: https://maven.apache.org/index.html.
Then install by copying to a directory of your choice and adding that directory to the PATH variable.

From the project directory you can use the following commands to build, test, and run.

There are JUnit tests which can be run with "mvn test".

The packages can be run locally with "mvn package" and then "java -cp target\schedule-api-1.0-SNAPSHOT.jar <fully qualified class name>",
in our case that is "com.pbussler.schedule.Demo" or "com.pubssler.schedule.Cli".

An independently executable JAR can be created with "mvn clean compile assembly:single",
which can then be run with "java -cp target\schedule-api-1.0-SNAPSHOT-jar-with-dependencies.jar".